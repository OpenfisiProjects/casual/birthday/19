## Zutaten:

Für die Chili Cheese Sauce:
- 2 EL Butter
- 2 EL Mehl
- 500 ml Milch
- 200 g Cheddar-Käse, gerieben
- 100 g Gouda-Käse, gerieben
- Salz, Pfeffer und Chilipulver
- 4 EL Jalapeños aus dem Glas, abgetropft und fein gehackt (Wasser aufbewahren)
    ### Optional:
    - 2x Tl Maisstärke
    - 1x Tl Jalapeño Wasser
    - Käsepulver
    <br>
    je nach gefühl, mehr Käse (Cheddar und Gouda)
    
Für die Burger:
- 12 Hamburger-Brötchen
- 1,2 kg Rinderhackfleisch
- Salz und Pfeffer
- 4 EL Öl
- 12 Scheiben Cheddar-Käse
- 12 Salatblätter
- 12 Tomatenscheiben

Für die Fries:
- 1 kg Kartoffeln, vorwiegend festkochend
- 3 EL Öl
- Salz, Pfeffer und Paprikapulver
- Trüffelsalz zum Würzen [(z.B. von Lacroix)](https://www.edeka.de/eh/suedwest/lacroix-trueffelsalz.jsp)

## Zubereitung:

1. Für die Chili Cheese Sauce die Butter in einem Topf schmelzen lassen. Das Mehl einrühren und kurz anschwitzen. Die Milch nach und nach unter Rühren dazugießen und aufkochen lassen. Die Hitze reduzieren und den Cheddar-Käse und den Gouda-Käse unterrühren, bis sie geschmolzen sind. Mit Salz, Pfeffer und Chilipulver abschmecken. Die Jalapeños unterheben und die Sauce warm halten.
2. Für die Burger das Hackfleisch mit Salz und Pfeffer würzen und zu 12 flachen Patties formen. In einer Pfanne das Öl erhitzen und die Patties von beiden Seiten je ca. 4 Minuten braten. In den letzten Minuten je eine Scheibe Cheddar-Käse auf die Patties legen und schmelzen lassen.
3. Die Brötchen aufschneiden und im Backofen oder Toaster leicht rösten. Die unteren Hälften mit etwas Chili Cheese Sauce bestreichen, mit Salatblättern, Tomatenscheiben und den Patties belegen. Die oberen Hälften daraufsetzen und leicht andrücken.
4. Für die Fries die Kartoffeln waschen, schälen und in dünne Stifte schneiden. In einer Schüssel mit Öl, Salz, Pfeffer und Paprikapulver vermengen. Ein Backblech mit Backpapier auslegen und die Kartoffelstifte darauf verteilen. Im vorgeheizten Backofen bei 200 °C Umluft ca. 20 Minuten knusprig backen.
5. Die Fries aus dem Ofen nehmen und mit Trüffelsalz würzen. Die Burger, Nuggets und Fries auf einer großen Platte anrichten und mit der restlichen Chili Cheese Sauce servieren. Dazu nach Belieben das Jalapeño-Wasser als Dip reichen.